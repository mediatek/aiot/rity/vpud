# Copyright (C) 2020  BayLibre, SAS
# Author: Fabien Parent <fparent@baylibre.com>

BINDIR ?= "/usr/bin"
LIBDIR ?= "/usr/lib"
VPUD_ARCH ?= aarch64
VPUD_SOC ?= mt8167
VPUD_PATH = ${VPUD_SOC}/${VPUD_ARCH}

all:

install:
	install -d ${BINDIR}
	install ${VPUD_PATH}/vpud ${BINDIR}

	install -d ${LIBDIR}
	install ${VPUD_PATH}/libvpud_vcodec.so ${LIBDIR}

	if [ -f "${VPUD_PATH}/libherope_sa.ca7.so" ]; then \
		install ${VPUD_PATH}/libherope_sa.ca7.so ${LIBDIR}; \
	fi

	if [ -f "${VPUD_PATH}/libvcodec_oal.so" ]; then \
		install ${VPUD_PATH}/libvcodec_oal.so ${LIBDIR}; \
	fi
	